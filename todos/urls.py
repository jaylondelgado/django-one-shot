from django.urls import path
from todos.views import TodoItemUpdateView, TodoItemCreateView, TodoListUpdateView, TodoListDeleteView, TodoListDetailView, TodoListCreateView, TodoListListView




urlpatterns = [
    path("", TodoListListView.as_view(), name="todo_list"),
    path("<int:pk>/", TodoListDetailView.as_view(), name='detail_list'),
    path("create_list/", TodoListCreateView.as_view(), name='create_list'),
    path("<int:pk>/update_list", TodoListUpdateView.as_view(), name="update_list"),
    path("<int:pk>/delete/", TodoListDeleteView.as_view(), name="delete_list"),
    path("<int:pk>/update_item", TodoItemUpdateView.as_view(), name="update_item"),
    path("create_item/", TodoItemCreateView.as_view(), name='create_item'),

]