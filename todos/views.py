from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from todos.models import TodoList, TodoItem
from django.urls import reverse_lazy

# Create your views here.
class TodoListListView(ListView):
    model = TodoList
    template_name = "list_list.html"
    context_object_name = "todo_list"



class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "create_list.html"
    fields = ["name"]
    success_url = reverse_lazy("details_list")


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "detail_list.html"
    context_object_name = "todo_list"


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "delete_list.html"


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "update_list.html"
    
    def get_success_url(self):
        return reverse_lazy("")

class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "create_item.html"
    fields = ["task", "due_date", "is_completed", "list"]


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "update_item.html"
    fields = ["task", "Do it", "due_date", "is_completed", "list"]