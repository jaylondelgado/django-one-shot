from django.db import models
from django.forms import CharField

# Create your models here.
class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(auto_now_add=False, null=True, blank=True)
    is_completed = models.BooleanField(null=False, default=False, blank=False)
    list = models.ForeignKey (
        "TodoList",
        related_name= "items",
        on_delete=models.CASCADE,
    )
    def __str__(self):
        return self.task

class TodoList(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    created_on = models.DateTimeField(auto_now_add=True, null=False, blank=True)

    def __str__(self):
        return self.name